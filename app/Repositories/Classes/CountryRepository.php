<?php


namespace App\Repositories\Classes;


use App\Exceptions\GeneralException;
use App\Models\Country;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class CountryRepository extends BaseRepository
{
    private $modelName = 'Country';


    public function __construct(Country $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     *
     * @return Country
     * @throws \Throwable
     * @throws \Exception
     */
    public function create(array $data): Country
    {
        return DB::transaction(function () use ($data) {

            $country = $this->model::create([
                'name' => $data['name']
            ]);

            if ($country) {
                return $country;
            }

            throw new GeneralException("There was a problem creating this :model. Please try again.", ['model' => $this->modelName]);
        });
    }

    /**
     * @param Country $country
     * @param array $data
     *
     * @return Country
     * @throws \Exception
     * @throws \Throwable
     * @throws GeneralException
     */
    public function update(Country $country, array $data): Country
    {
        if ($country->update($data)) {
            return $country;
        }

        throw new GeneralException("There was a problem updating this :model. Please try again.", ['model' => $this->modelName]);

    }

    public function findOrCreate(string $country_name)
    {
        try {
            $country = $this->model->where(['name' => $country_name])->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            $country = $this->create(['name' => $country_name]);
        }

        return $country;
    }

}
