<?php


namespace App\Repositories\Classes;


use App\Exceptions\GeneralException;
use App\Models\CovidData;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class CovidDataRepository extends BaseRepository
{
    private $modelName = 'CovidData';


    public function __construct(CovidData $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     *
     * @return CovidData
     * @throws \Throwable
     * @throws \Exception
     */
    public function create(array $data): CovidData
    {
        return DB::transaction(function () use ($data) {

            $covid_data = $this->model::create([
                'country_id' => $data['country_id'],
                'date' => $data['date'],
                'confirmed_injuries' => $data['confirmed_injuries']
            ]);

            if ($covid_data) {
                return $covid_data;
            }

            throw new GeneralException("There was a problem creating this :model. Please try again.", ['model' => $this->modelName]);
        });
    }

    /**
     * @param CovidData $covid_data
     * @param array $data
     *
     * @return CovidData
     * @throws \Exception
     * @throws \Throwable
     * @throws GeneralException
     */
    public function update(CovidData $covid_data, array $data): CovidData
    {
        if ($covid_data->update($data)) {
            return $covid_data;
        }

        throw new GeneralException("There was a problem updating this :model. Please try again.", ['model' => $this->modelName]);

    }

}
