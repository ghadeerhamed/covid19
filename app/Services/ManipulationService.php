<?php


namespace App\Services;


use App\Repositories\Classes\CountryRepository;
use App\Repositories\Classes\CovidDataRepository;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class ManipulationService
{

    private $country_repository;
    private $covid_data_repository;
    /**
     * @var array
     * Contains columns names
     */
    private $headers = [];

    const DATA_REPO_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";

    public function __construct(CountryRepository $country_repository, CovidDataRepository $covid_data_repository)
    {
        $this->country_repository = $country_repository;
        $this->covid_data_repository = $covid_data_repository;
    }

    public function start_manipulation()
    {
        Log::info("Start Manipulation...");
        $data = $this->get_data_from_github();
        $res = $this->manipulate($data);
//        Log::info("Added new " . $res['scs'] . " rows, And failed to add " . $res['fld'] . " rows.");
        if ($res['scs']) Log::info($res['scs']);
        if ($res['fld']) Log::alert($res['fld']);
        if ($res['msg']) Log::info($res['msg']);
    }

    private function get_data_from_github()
    {
        $client = new Client();
        $data = $client->get(self::DATA_REPO_URL);
        return $data->getBody()->getContents();
    }

    private function manipulate($data)
    {
        $lines = explode("\n", $data); // Get each separated Line as an array item.

        array_pop($lines); //Last line is always an empty line.
        $data = [];
        foreach ($lines as $line) {
            $line_arr = str_getcsv($line); //Convert csv line into array
            unset($line_arr[0], $line_arr[2], $line_arr[3]); // Remove unused columns.
            $line_arr = array_values($line_arr); // Reindex array after removing items.

            // Save columns names and continue to data lines
            if (!$this->headers) {
                $this->headers = $line_arr;
                continue;
            }
            //Convert indexed array to assoc array like ['country' => country, 'day1' => Day1 data, 'day2' => Day2 data ....].
            $data[] = $this->build_array_line($line_arr);
        }

        //Data to be non-cumulative.
        $data = $this->non_cumulative($data);

        //Saving results to our DataBase.
        return $this->save_to_db($data);
    }

    private function build_array_line(array $line)
    {
        $array = [];

        for ($i = 0; $i < sizeof($this->headers); $i++) {
            $array[$this->headers[$i]] = $line[$i];
        }

        return $array;
    }

    /**
     * @param array $data
     * @return array
     * Returned array contains the data for each country as ['country name1' => [ 'day1' => value, 'day2' => value ..], ...]
     *
     */
    private function non_cumulative(array $data)
    {
        $array = [];
        foreach ($data as $datum) {
            $country = array_shift($datum);
            if (!isset($array[$country]))
                $array[$country] = [];
            $pre_key = null;
            foreach ($datum as $key => $value) {
                $date = Carbon::parse($key)->format("d-m-Y");
                if ($pre_key) {
                    $pre_val = $datum[$pre_key];
                    $new_val = $value - $pre_val;
                } else {
                    $new_val = (int)$value;
                }
                $pre_key = $key;
                if (!isset($array[$country][$date]))
                    $array[$country][$date] = $new_val;
                else
                    $array[$country][$date] += $new_val;
            }
        }
        return $array;
    }

    private function save_to_db(array $data)
    {
        $scs = $fld = $msg = ''; //results log
        try {
            $last_date = $this->covid_data_repository->orderBy('id', 'desc')->first()->date;
        } catch (ModelNotFoundException $exception) {
            $last_date = Carbon::now()->addYears(-100)->format("d-m-Y");
        }

        foreach ($data as $country_name => $values) {
            $country = $this->country_repository->findOrCreate($country_name);
            $data_to_insert = [];
            foreach ($values as $date => $value) {
                if (Carbon::parse($last_date) > Carbon::parse($date))
                    continue;
                if ($country->covid_data()->where('date', $date)->count()) {
                    continue;
                }
                $data_to_insert[] = [
                    'country_id' => $country->id,
                    'date' => $date,
                    'confirmed_injuries' => $value,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }

            if ($data_to_insert) {
                $add = $country->covid_data()->insert($data_to_insert);
                if ($add)
                    $scs .= "Country's '$country_name': Data added successfully [" . sizeof($data_to_insert) . "] rows." . PHP_EOL;
                elseif (!$add)
                    $fld .= "Country's '$country_name': Data addition failed [" . sizeof($data_to_insert) . "] rows." . PHP_EOL;
            } else
                $msg .= "Country's '$country_name': No new data found!." . PHP_EOL;

        }
        return ['scs' => $scs, 'fld' => $fld, 'msg' => $msg];
    }
}
