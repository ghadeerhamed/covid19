<?php

namespace App\Console\Commands;

use App\Services\ManipulationService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class StartManipulate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manipulation:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start Get covid19 data from github repo then proccess it and save to database.';


    private $manipulation_service;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ManipulationService $manipulation_service)
    {
        parent::__construct();
        $this->manipulation_service = $manipulation_service;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '3g');
        ini_set('max_execution_time', 0);

        $start_time = microtime(true);
        $this->manipulation_service->start_manipulation();
        $end_time = microtime(true);
        $execution_time = ($end_time - $start_time);
        $msg = "Execution time " . $execution_time . " sec.";
        Log::info($msg);
        return 1;
    }
}
