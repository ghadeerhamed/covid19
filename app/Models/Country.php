<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 * @package App\Models
 *
 * @property integer $id
 * @property String $name
 * @property-read CovidData[] | Collection covid_data
 */
class Country extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function covid_data()
    {
        return $this->hasMany(CovidData::class);
    }
}
